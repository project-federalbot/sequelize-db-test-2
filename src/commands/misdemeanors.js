module.exports = {
    name: 'misdemeanors',
    desc: 'Find the amount of misdemeanors on you or another member.',
    usage: ';misdemeanor [member]',
    examples: ';misdemeanors // Get your current misdemeanors\n\n;misdemeanors @sperg#6969 // Get sperg\'s current misdemeanors',
    async execute(msg, bot, args) {
        const Discord = require('discord.js');
        const mrmeanor = await bot.Misdemeanors.findOne({
            where: {
                name: msg.mentions.members.first().user.id || msg.author.id,
                guild: msg.guild
            }
        });
        if (!mrmeanor) return msg.channel.send("Couldn't find any misdemeanors.");
        const mEmbed = new Discord.RichEmbed().setDescription(`${msg.author.tag}, ${msg.author.id===mrmeanor.name ? `you have ` : `${msg.mentions.members.first().displayName} has `} **${mrmeanor.misdemeanors}** misdemeanors.`).setAuthor(msg.author, msg.author.displayAvatarURL, msg.author.displayAvatarURL).setTimestamp();
        msg.channel.send(mEmbed);
    }
};